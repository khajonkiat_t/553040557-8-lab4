package tantiwetchayanon.khajonkiat.lab4;

public class ThaiPlayer extends Player {
	private String SportName;
	
	ThaiPlayer(String name,int bDay,int bMonth,int bYear,double weight,double height)
	{
		super (name, bDay, bMonth, bYear, weight, height);
	}

	public void setSportName(String SportName) 
	{
		// TODO Auto-generated method stub
		this.SportName = SportName; 
	}
	public String getSportName()
	{
		return SportName ; 
	}
	
	public String toString()
	{
		return "Player[" +name+ " is " + calage(bDay,bMonth,bYear) +  " year old,with weight = " +weight+ " and heigh " +height+ " ]\n " + "[nationality = thai" + ((SportName != null)? "award " + SportName: "") + " ]" ;
	}
	
	}
