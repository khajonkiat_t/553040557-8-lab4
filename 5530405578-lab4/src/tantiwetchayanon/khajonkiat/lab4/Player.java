package tantiwetchayanon.khajonkiat.lab4;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Player 
{
	protected String name;
	protected int bDay;
	protected int bMonth;
	protected int bYear;
	protected double weight;
	protected double height;
	
	Player(String name,int bDay,int bMonth,int bYear,double weight,double height)
	{
		this.name = name;
		this.bDay = bDay;
		this.bMonth = bMonth;
		this.bYear = bYear;
		this.weight = weight;
		this.height = height;
	}

	
	 public double calage( int bDay, int bMonth, int bYear) 
	 {
		    Calendar cal = new GregorianCalendar(bYear, bMonth, bDay);
		    Calendar now = new GregorianCalendar();
		    int res = now.get(Calendar.YEAR) - cal.get(Calendar.YEAR);
		    if ((cal.get(Calendar.MONTH) > now.get(Calendar.MONTH))
		        || (cal.get(Calendar.MONTH) == now.get(Calendar.MONTH) && cal.get(Calendar.DAY_OF_MONTH) > now
		            .get(Calendar.DAY_OF_MONTH))) 
		    { 
		    	res--;
		    }		      
		      return res;
	}
	 
	public String toString()
	{
		return "Player[" +name+ " is" + calage(bDay,bMonth,bYear) +  "year old,with weight = " +weight+ "and heigh " +height+ " ]";
	}
	public void setWeight(double weight1) {
		// TODO Auto-generated method stub
		weight = weight1;
	}


	public double getWeight() {
		// TODO Auto-generated method stub
		return weight;
	}


	public double getHeigh() {
		// TODO Auto-generated method stub
		return height;
	}


	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}
	
	
}



